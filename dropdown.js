import "./dropdown.scss";

export default class DropDown {
    constructor(elem = null) {
        this.elem = elem;
        this.parent = document.createElement('div');
        this.parent.classList.add('drop-wrapper');
        this.elem.parentNode.insertBefore(this.parent, this.elem);
        this.parent.appendChild(this.elem);
        this.height = 0;
    }
    init() {
        if (!this.elem) return;
        this.elem.style.display = 'none';
        const button = document.createElement('button');
        button.classList.add("btn", "drop-toggle", "btn-light");
        button.autocomplete = "off";
        button.innerHTML = '<div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"></div></div></div>';
        const inner = button.querySelector('.filter-option-inner-inner');
        this.elem.after(button);
        const option = this.elem.querySelectorAll('option');
        const selected = [...option].filter((el) => { return el.selected; })[0] || null;


        if (!selected) {
            this.pastDefault(inner);
            return;
        }

        const img = selected.getAttribute('data-img') || null;

        if (img) {
            inner.innerHTML = `<div class="picker-img" style="background-image: url(${img})"></div>`;
        }

        inner.innerHTML += `<div class="value">${selected.value}</div>`

        const dropdown = document.createElement('div');
        dropdown.innerHTML = '<div class="inner" role="listbox" aria-expanded="false" tabindex="-1"><ul class="inner-drop-menu inner"></ul></div>';
        dropdown.classList.add('drop-menu');
        dropdown.setAttribute('role', 'combobox')


        button.after(dropdown);
        const ul = dropdown.querySelector('ul');

        [...option].reduce((html, opt) => {
            const noda = document.createElement('li');
            if (opt.value === selected.value) noda.classList.add('selected', 'active');
            noda.target_value = opt.value;
            noda.img_value = (opt.getAttribute('data-img')) ? opt.getAttribute('data-img') : null;
            noda.innerHTML = `<a role="option" class="drop-item ${opt.value === selected.value ? 'selected active' : ''}" aria-disabled="false" tabindex="0" aria-selected="${opt.value === selected.value}">
            ${opt.getAttribute('data-img') ?
                    `<div class="picker-img" style="background-image: url(${opt.getAttribute('data-img')});"></div><span class="text">${opt.innerText}</span>`
                    :
                    `<span class="text">${opt.innerText}</span>`
                }</a>`;
            return ul.appendChild(noda);
        }, '');

        const li = ul.querySelectorAll('li');

        [...li].map((el_li) => {
            let that = this;
            el_li.addEventListener('click', function () {
                let val = this.target_value;
                that.elem.value = val;
                that.elem.dispatchEvent(new Event('change'));
            });
        });

        this.height = dropdown.offsetHeight;

        dropdown.style.height = 0;
        dropdown.style.display = 'none';

        const open = () => {
            dropdown.style.display = '';
            dropdown.classList.add('animation-progress');
            dropdown.classList.remove('show');
            dropdown.style.position = "absolute";


            this.request_anim = this.animate({
                duration: 300,
                timing: function (timeFraction) {
                    return Math.pow(timeFraction, 5);
                },
                draw: (progress) => {
                    dropdown.style.height = progress * this.height + 'px';
                },
                startDraw: () => {
                    dropdown.style.display = '';
                    this.onOpen();
                },
                endDraw: () => {
                    dropdown.classList.add('show');
                    dropdown.classList.remove('animation-progress');
                    inner_onclick(dropdown);
                }
            });
        }
        const close = () => {
            if (!dropdown.classList.contains('show')) return;
            dropdown.classList.remove('show');
            dropdown.classList.add('animation-progress');
            this.request_anim = this.animate({
                duration: 300,
                timing: function (timeFraction) {
                    return Math.pow(timeFraction, 5);
                },
                draw: (progress) => {
                    dropdown.style.height = this.height - progress * this.height + 'px';
                    if (progress === 1) dropdown.style.height = '0px';
                },
                startDraw: () => {
                    dropdown.style.display = '';
                    
                },
                endDraw: () => {
                    dropdown.classList.remove('animation-progress');
                    dropdown.style.position = "";
                    dropdown.style.display = 'none';
                    inner_onclick(dropdown);
                }
            });
        }

        const inner_onclick = (dropdown) => {

            button.onclick = () => {
                button.onclick = () => { };
                (!dropdown.offsetHeight) ? open(dropdown) : close(dropdown);
            }
        }

        document.addEventListener('click', () => {
            if (dropdown.classList.contains('animation-progress')) return;
            close(dropdown);
        });

        inner_onclick(dropdown);

        this.elem.addEventListener('change', () => {
            [...li].map((el_li) => {
                el_li.classList.remove('selected', 'active');
                if (el_li.target_value === this.elem.value) {
                    el_li.classList.add('selected', 'active');
                    if (el_li.img_value) {
                        inner.innerHTML = `<div class="picker-img" style="background-image: url(${el_li.img_value})"></div>`;
                    }

                    inner.innerHTML += `<div class="value">${el_li.target_value}</div>`

                }
            });
            this.onChange();
        })
        this.onInit();
    }
    onChange() { };
    onOpen() { };
    onInit() { };


    pastDefault(inner) {
        inner.innerHTML = `<div class="value">Умолчание</div>`;
    }

    animate({ timing = () => { }, duration = 10, draw = () => { }, endDraw = () => { }, startDraw = () => { } }) {
        var start = performance.now();
        startDraw();
        return requestAnimationFrame(function animate(time) {
            // timeFraction от 0 до 1
            var timeFraction = (time - start) / duration;
            if (timeFraction > 1) timeFraction = 1;

            // текущее состояние анимации
            var progress = timing(timeFraction)

            draw(progress);

            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            }
            else {
                endDraw();
            }
        });
    }
}